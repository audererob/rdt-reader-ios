//
//  Copyright (C) 2019 University of Washington Ubicomp Lab
//  All rights reserved.
//
//  This software may be modified and distributed under the terms
//  of a BSD-style license that can be found in the LICENSE file.
//

#import <Foundation/Foundation.h>
#import "ShowPhotoViewController.h"




@implementation ShowPhotoViewController

// This is the back button
// Everything it is pressed it dismisses
// the view
- (IBAction)backbutton:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];

    
}

- (IBAction)saveImage:(id)sender {
    UIImageWriteToSavedPhotosAlbum(self.image, nil, nil, nil);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // This shows the imagee of the RDT
    // In the UIImageview
    [self.imgView setImage:self.image];
    [self.imgView setContentMode:UIViewContentModeScaleAspectFit];
    [self.resultView setImage:self.resultImage];
    [self.resultView setContentMode:UIViewContentModeScaleAspectFit];
    [self.timeLabel setText:self.timeText];
    [self.controlLabel setText:self.control];
    [self.testALabel setText:self.testA];
    [self.testBLabel setText:self.testB];
}

- (void)setImage:(UIImage *)image {
    if (image.size.width > image.size.height) {
        UIImage * portraitImage = [[UIImage alloc] initWithCGImage: image.CGImage
                                                             scale: 1.0
                                                       orientation: UIImageOrientationRight];
        _image = portraitImage;
    } else {
        _image = image;
    }
}

- (void)setResultImage:(UIImage *)image {
    if (image.size.width > image.size.height) {
        UIImage * portraitImage = [[UIImage alloc] initWithCGImage: image.CGImage
                                                             scale: 1.0
                                                       orientation: UIImageOrientationRight];
        _resultImage = portraitImage;
    } else {
        _resultImage = image;
    }
}

@end
