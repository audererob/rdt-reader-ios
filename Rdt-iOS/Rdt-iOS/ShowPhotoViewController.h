//
//  Copyright (C) 2019 University of Washington Ubicomp Lab
//  All rights reserved.
//
//  This software may be modified and distributed under the terms
//  of a BSD-style license that can be found in the LICENSE file.
//

#import <UIKit/UIKit.h>

@interface ShowPhotoViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIImageView *imgView;
@property (nonatomic, retain) IBOutlet UIImageView *resultView;
@property (nonatomic, retain) IBOutlet UILabel *timeLabel;
@property (nonatomic, retain) IBOutlet UILabel *testALabel;
@property (nonatomic, retain) IBOutlet UILabel *testBLabel;
@property (nonatomic, retain) IBOutlet UILabel *controlLabel;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) UIImage *resultImage;
@property (nonatomic, retain) NSString *timeText;
@property (nonatomic, retain) NSString *testB;
@property (nonatomic, retain) NSString *testA;
@property (nonatomic, retain) NSString *control;

- (void)setImage:(UIImage *)image;
- (void)setResultImage:(UIImage *)image;
@end
